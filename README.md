# EfficientNet fine-tuning on Kubernetes using TFJob

This project demonstrates using `TFJob` with the [Kubeflow Training Operator](https://github.com/kubeflow/tf-operator) to perform distributed model training with Tensorflow on Kubernetes. The data augmentation and model training code is taken from this [Keras Code Example](https://keras.io/examples/vision/image_classification_efficientnet_fine_tuning/).

The classifier model is tuned to the [Standford Dogs Dataset](http://vision.stanford.edu/aditya86/ImageNetDogs/) which is a part of [TensorFlow Datasets](https://www.tensorflow.org/datasets).


## Run on local Mac with M1 Apple Silicon chip

```bash
brew install miniforge cmake boost openal-soft sdl2

conda create -n ray python=3.8 
conda activate ray
conda install -c apple gputil lz4 hiredis grpcio numpy pandas scikit-learn scikit-image tensorflow-deps matplotlib

pip install tensorflow-macos
pip install tensorflow-metal

pip install dm_tree atari_py tensorboardx

# For some reason, ray breaks the numpy install with pip
conda install --force-reinstall -c apple numpy

pip install tensorflow-datasets
```

You need to modify the code to make it work with this workaround:

```python
    with tf.device("/CPU:0"):
        model = build_model()
```

Even though it indicates CPU, it will use the GPU if possible.